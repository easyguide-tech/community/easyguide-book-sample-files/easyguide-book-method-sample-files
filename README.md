Easy Guide ! a new method - EasyGuide.tech
===
Source template files
---

The package is Open core Source released under the [MIT Expat](LICENSE) with DCO requirement for contributing (Developer Certificate of Origin).

It is developed by cheikhna diouf and derived from his official book "Professional Markdown for developers : Part 1 Onboarding" which you can buy as printed book or as ebook on official website, Amazon, Google Play and the iBooks Store.

* Website : https://easyguide.tech

# Copyright and License
---
* This project is released under the MIT expat license - see the [LICENSE.md](LICENSE.md) file for details.