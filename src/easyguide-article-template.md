Title
====

Subtitle
---

**New rules** and **new éléments** to help reduce the learning curve of any technical documentations.

The **EasyGuide methodology** is a **feature-by-feature practical approach** for easy and rapid learning of the essentials, the key concepts, the most frequent and common use cases.


# Introduction

---

**Theme:**
**Subject:**
**Interest:**
**Plan:** 

- Navigation
- Business requirement
- Essential features
- Frequent procedures
- Material details

# Table of contents

---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Introduction](#introduction)
- [Table of contents](#table-of-contents)
- [BUSINESS REQUIREMENT](#business-requirement)
  - [ESSENTIAL FEATURE](#essential-feature)
    - [FREQUENT PROCEDURE](#frequent-procedure)
- [CONCLUSION](#conclusion)
- [FOOTNOTE: MATERIAL DETAILS](#footnote-material-details)
  - [Description](#description)
  - [Dictionary](#dictionary)
  - [Link](#link)
  - [Image](#image)
  - [Schema](#schema)
  - [Blockcode](#blockcode)
  - [Table](#table)
  - [Navigation: Table of contents](#navigation-table-of-contents)
  - [Blockquote](#blockquote)
  - [Alert notes](#alert-notes)
  - [FootNote](#footnote)
  - [Meta data](#meta-data)

<!-- /code_chunk_output -->

# BUSINESS REQUIREMENT

---

## ESSENTIAL FEATURE

---

### FREQUENT PROCEDURE

---  

1. **State:**
2. **Event:**
3. **Task:**

# CONCLUSION

---

**Resume:**

**Outline:**

---

# FOOTNOTE: MATERIAL DETAILS

## Description

It is so easy to break down and destroy. The heroes are those who make peace and build - Nelson Mandela

## Dictionary

term1
: description term

term2
: description term

term3
: description term

## Link

[Link EasyGuide](https://easyguide.tech)

## Image

![logo image](assets/images/logo.png "Logo title text")

* **Image with external link** to a video
[![Moonwalker](http://img.youtube.com/vi/9AjkUyX0rVw/0.jpg)](https://www.youtube.com/watch?v=9AjkUyX0rVw)
(We Are The World album cover - http://usaforafrica.org/ )

## Schema

![diagram image](assets/images/diagram.png "Diagram title text")


**Includes 7 types of schema as templates :**

  1. **Contextual map** diagram for plans and overviews

  2. **Conceptual story** diagram for features or entities and relations

  3. **Material package** diagram for resources and dependencies

  4. **Structural tree** diagram for hierarchic data

  5. **Sequential flow** diagram for processes and procedures

  6. **Metrics chart** for KPI monitorings

  7. **Shape board** for visual profiles and blueprints

## Blockcode

```js
console.log("It is so easy to break down and destroy. The heroes are those who make peace and build - Nelson Mandela");
```

## Table

| head left | title centered | title right |
| :---- | :-----: | -----: |
| row | value | value |
| another row | another value | another value |

## Navigation: Table of contents

- [1 REQUIREMENT](#1-requirement)
  - [1.1 FEATURE](#11-feature)
    - [1.1.1 PROCEDURE](#111-procedure)
- [2 REQUIREMENT](#2-requirement)
  - [2.1 FEATURE](#21-feature)
    - [2.1.1 PROCEDURE](#211-procedure)

## Blockquote

> "It is so easy to break down and destroy. The heroes are those who make peace and build" - Nelson Mandela
> http://www.mandela.gov.za


## Alert notes

> **(v) Success note:**
Good practices, styleguide
For succesful or positive actions

> **(i) Info note:**
Notify important notes
Details, neutral informative message

> **(!) Warning note:**
Warn to better check X factors
That might need your attention


> **(x) Danger note:**
prevent against dangerous or potentially negative actions


> **(?) User note:**
Your notes from your own practices :
.................................................................................................
.................................................................................................
.................................................................................................


## FootNote

Here's a sentence with a footnote. [^1]
  
[^1]: Lorem ipsum https://easyguide.tech


## Meta data

- **Author:** Cheikhna Diouf - Tech lead
- **Contributor:** EasyGuide.tech contributors
- **Company:** EasyGuide.tech
- **License:** Copyright (c) 2020-present Cheikhna Diouf - Cheikhnadiouf.com, <contact@cheikhnadiouf.com> (Cheikhnadiouf.com)
- **License:** Copyright (c) 2020-present EasyGuide.tech, Inc. <info@easyguide.tech> (easyguide.tech)
- **License:** MIT License (Expat)
- **Source:** [EasyGuide.tech](https://easyguide.tech)
- **Version:** 1.0.0
- **tags:** easy, guide, manual, user guide, tutorial, quickstart, readme, onboarding, guideline, cheat sheet, document, handbook, book, learn, learning, formation, training, skill, school, lesson, course, how, how to, technologie, markdown


